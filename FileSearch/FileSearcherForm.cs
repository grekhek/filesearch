﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics;

namespace FileSearch
{
    public partial class FileSearcher : Form
    {
        private delegate void delUpdateTB(TextBox tb, string str); //делегат для обновления текстовых полей
        private delegate void delUpdateDirView(string str); //делегат для обновления дерева директорий
        private delegate void delClearDirView(); //делегат для очищения дерева директорий
        private delegate void delButtonVisibility(bool bv); //делегат для обновления видимости кнопки
        private delegate void delTBAccess(bool tba); //делегат для обновления видимости кнопки
        private delegate void delSetButtonText(Button b, string t); //делегат для обновления названия кнопки кнопки
        BackgroundWorker searcher; //фоновый поиск

        private int filesProcessedCount;
        private int filesFoundCount;

        private readonly Stopwatch sw = new Stopwatch(); //для измерения времени работы

        private bool buttonPauseClicked = false; //для состояния паузы\работы поиска

        //обновление текстового поля
        private void updateTB(TextBox tb, string str)
        {
            tb.Text = str;
            tb.Update();
        }

        //очищение дерева директорий
        private void clearDirView()
        {
            treeViewDirectories.Nodes.Clear();
        }

        //настройка видимости кнопки паузы
        private void setButtonVisibility(bool bv)
        {
            buttonPause.Visible = bv;
        }

        //настройка текста кнопки
        private void setButtonText(Button b, string t)
        {
            b.Text = t;
        }

        //настройка редактируемости полей критериев
        private void setTBAccess(bool tba)
        {
            textBoxDirectory.Enabled = tba;
            textBoxText.Enabled = tba;
            textBoxTemplate.Enabled = tba;
        }

        //обновление дерева директорий по пути к файлу, который записан в строке
        private void updateDirView(string str)
        {
            string[] names = str.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries); //разбиение пути на части
            TreeNode node = null;
            for (int j = 0; j < names.Length; j++)
            {
                TreeNodeCollection nodes;
                if (node == null)
                {
                    nodes = treeViewDirectories.Nodes;
                }
                else
                {
                    nodes = node.Nodes;
                }
                node = FindNode(nodes, names[j]);//поиск узла в дереве
                if (node == null)
                {
                    node = nodes.Add(names[j]);//если узел не найден, то добавить узел

                    //настройка иконок дерева директорий
                    if(j == 0)
                    {
                        node.ImageIndex = 0;
                    }
                    else if (j < names.Length - 1)
                    {
                        node.ImageIndex = 1;
                    }
                    else
                    {
                        node.ImageIndex = 2;
                    }
                }
            }
            treeViewDirectories.Update();
            treeViewDirectories.ExpandAll();
        }

        //поиск узла дерева
        private TreeNode FindNode(TreeNodeCollection nodes, string p)
        {
            for (int i = 0; i < nodes.Count; i++)
                if (nodes[i].Text.ToLower(CultureInfo.CurrentCulture) == p.ToLower(CultureInfo.CurrentCulture))
                {
                    return nodes[i];
                }
            return null;
        }

        //поиск файлов
        private void GetFiles(string directory)
        {
            //делегаты для обновлений\очищений элементов окна программы
            delUpdateTB delBox = new delUpdateTB(updateTB);
            delUpdateDirView delView = new delUpdateDirView(updateDirView);

            //string directory = textBoxDirectory.Text; //директория поиска
            string template = textBoxTemplate.Text; //шаблон имени файла
            string filetexttemplate = textBoxText.Text; //текст встречающийся в файле

            //регулярные выражения для поиска файлов
            Regex matchInFile = new Regex(filetexttemplate);
            Regex matchFileName = new Regex("^" + template.Replace("*", ".*") + "$");

            DirectoryInfo dirInfo = new DirectoryInfo(directory); //информация о директории поиска

            

            //int filesProcessedCount = 0, filesFoundCount = 0; //для подсчета количества найденных\обработанных файлов


            foreach(DirectoryInfo dir in dirInfo.GetDirectories())
            {
                try
                {
                    if (!searcher.CancellationPending)
                    {
                        GetFiles(dir.FullName);
                    }
                }
                catch(Exception error)
                {

                }
            }
            //просмотр каждого файла в поисковой директории включая все поддиректории
            foreach (FileInfo fi in dirInfo.GetFiles("*"))
            {
                //если нажата кнопка паузы, то до тех пор пока она не будет нажата снова или не будет нажата кнопка остановка, будет выпорняться этот цикл
                while(buttonPauseClicked)
                {
                    if(searcher.CancellationPending)
                    {
                        break;
                    }
                }

                //если не нажата кнопка стоп, то выполнять поиск
                if (!searcher.CancellationPending)
                {
                    this.Invoke((Action)(() => delBox(textBoxCurrentFile,fi.FullName))); //текущий обрабатываемый файл отображается в поле текста
                    
                    string curfilename = Path.GetFileName(fi.FullName); //получение имени файла без пути к нему

                    //если найдено совпадение в имени, то проверить содержимое файла на совпадение
                    if (matchFileName.IsMatch(curfilename))
                    {
                        string curfiletext = File.ReadAllText(fi.FullName);

                        //если найдено совпадение в тексте файла, то обновить дерево директорий и счетчик найденных файлов
                        if (matchInFile.IsMatch(curfiletext))
                        {
                            filesFoundCount++;
                            this.Invoke((Action)(() => delView(fi.FullName)));
                            this.Invoke((Action)(() => delBox(textBoxFilesFound, filesFoundCount.ToString())));
                        }
                    }
                    filesProcessedCount++;
                    this.Invoke((Action)(() => delBox(textBoxFileCount, filesProcessedCount.ToString()))); //обновить счетчик обработанных файлов
                }
                else
                {
                    break;
                }
            }
        }

        //кнопка поиска или остановки поиска
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            //если фоновый поиск работает, то 
            if (searcher.IsBusy)
            {
                //разрешиется редактирование текстовых полей
                textBoxDirectory.Enabled = true;
                textBoxText.Enabled = true;
                textBoxTemplate.Enabled = true;
                
                searcher.CancelAsync(); //остановка поиска
                timerElapsed.Stop(); //остановка таймера
                
                buttonSearch.Text = "Search"; //изменение названия кнопки
                buttonPause.Visible = false;
            }
            else //если фоновый поиск не работает, то
            {
                //запретить редактирование текстовых полей
                textBoxDirectory.Enabled = false;
                textBoxText.Enabled = false;
                textBoxTemplate.Enabled = false;

                buttonPauseClicked = false; //переустановить значение паузы
                timerElapsed.Start(); //запустить таймер
                searcher.RunWorkerAsync(); //запустить поиск

                //изменить названия кнопок
                buttonPause.Text = "Pause";
                buttonPause.Visible = true;
                buttonSearch.Text = "Stop";
            }
        }

        //кнопка паузы
        private void buttonPause_Click(object sender, EventArgs e)
        {
            if (buttonPauseClicked)
            {
                sw.Start();
                buttonPause.Text = "Pause";
            }
            else
            {
                sw.Stop();
                buttonPause.Text = "Continue";
            }
            buttonPauseClicked = !buttonPauseClicked;
        }

        //фоновый поиск
        private void searcher_DoWork(object sender, DoWorkEventArgs e)
        {
            delButtonVisibility delBV = new delButtonVisibility(setButtonVisibility);
            delTBAccess delTBA = new delTBAccess(setTBAccess);
            delSetButtonText delSBT = new delSetButtonText(setButtonText);
            delUpdateTB delBox = new delUpdateTB(updateTB);
            delClearDirView delClearView = new delClearDirView(clearDirView);

            if (searcher.CancellationPending)
            {
                e.Cancel = true;
            }
            
            sw.Restart();
            string directory = textBoxDirectory.Text; //директория поиска
            filesProcessedCount = 0;
            filesFoundCount = 0;
            //очищение элементов окна перед запуском поиска
            this.Invoke((Action)(() => delBox(textBoxCurrentFile, "")));
            this.Invoke((Action)(() => delClearView()));

            GetFiles(directory);
            sw.Stop();
            
            //разрешиется редактирование текстовых полей
            this.Invoke((Action)(() => delTBA(true)));

            this.Invoke((Action)(() => delSBT(buttonSearch, "Search"))); //изменение названия кнопки
            this.Invoke((Action)(() => delBV(false)));
        }

        //счетчик таймера
        private void timerElapsed_Tick(object sender, EventArgs e)
        {
            delUpdateTB delBox = new delUpdateTB(updateTB);
            this.Invoke((Action)(() => delBox(textBoxTimer, sw.Elapsed.ToString(@"hh\:mm\:ss\.f"))));
        }

        //загрузка прошлых значений критериев поиска
        private void FileSearcher_Load(object sender, EventArgs e)
        {
            textBoxDirectory.Text = Properties.Settings.Default.textBoxDirectory;
            textBoxTemplate.Text = Properties.Settings.Default.textBoxTemplate;
            textBoxText.Text = Properties.Settings.Default.textBoxText;
        }

        //сохранение прошлых значений критериев поиска
        private void FileSearcher_FormClosing(object sender, FormClosingEventArgs e)
        {
            searcher.CancelAsync();
            searcher.Dispose();
            Properties.Settings.Default.textBoxDirectory = textBoxDirectory.Text;
            Properties.Settings.Default.textBoxTemplate = textBoxTemplate.Text;
            Properties.Settings.Default.textBoxText = textBoxText.Text;
            Properties.Settings.Default.Save();
        }

        public FileSearcher()
        {
            InitializeComponent();
            searcher = new BackgroundWorker();
            searcher.DoWork += searcher_DoWork;
            searcher.WorkerSupportsCancellation = true;
        }
    }
}
