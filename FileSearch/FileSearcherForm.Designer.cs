﻿namespace FileSearch
{
    partial class FileSearcher
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileSearcher));
            this.labelDirectory = new System.Windows.Forms.Label();
            this.labelTemplate = new System.Windows.Forms.Label();
            this.labelText = new System.Windows.Forms.Label();
            this.textBoxText = new System.Windows.Forms.TextBox();
            this.textBoxTemplate = new System.Windows.Forms.TextBox();
            this.textBoxCurrentFile = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.labelCurrentFile = new System.Windows.Forms.Label();
            this.textBoxDirectory = new System.Windows.Forms.TextBox();
            this.treeViewDirectories = new System.Windows.Forms.TreeView();
            this.buttonPause = new System.Windows.Forms.Button();
            this.textBoxTimer = new System.Windows.Forms.TextBox();
            this.timerElapsed = new System.Windows.Forms.Timer(this.components);
            this.textBoxFileCount = new System.Windows.Forms.TextBox();
            this.labelTimer = new System.Windows.Forms.Label();
            this.labelFileCount = new System.Windows.Forms.Label();
            this.textBoxFilesFound = new System.Windows.Forms.TextBox();
            this.labelFilesFound = new System.Windows.Forms.Label();
            this.tableLayoutPanelApp = new System.Windows.Forms.TableLayoutPanel();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanelApp.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelDirectory
            // 
            this.labelDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDirectory.AutoSize = true;
            this.labelDirectory.Location = new System.Drawing.Point(3, 0);
            this.labelDirectory.Name = "labelDirectory";
            this.labelDirectory.Size = new System.Drawing.Size(84, 28);
            this.labelDirectory.TabIndex = 3;
            this.labelDirectory.Text = "Directory:";
            this.labelDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTemplate
            // 
            this.labelTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTemplate.AutoSize = true;
            this.labelTemplate.Location = new System.Drawing.Point(3, 28);
            this.labelTemplate.Name = "labelTemplate";
            this.labelTemplate.Size = new System.Drawing.Size(84, 28);
            this.labelTemplate.TabIndex = 4;
            this.labelTemplate.Text = "File name:";
            this.labelTemplate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelText
            // 
            this.labelText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(3, 56);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(84, 28);
            this.labelText.TabIndex = 5;
            this.labelText.Text = "File context:";
            this.labelText.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxText
            // 
            this.textBoxText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelApp.SetColumnSpan(this.textBoxText, 3);
            this.textBoxText.Location = new System.Drawing.Point(93, 60);
            this.textBoxText.Name = "textBoxText";
            this.textBoxText.Size = new System.Drawing.Size(264, 20);
            this.textBoxText.TabIndex = 2;
            // 
            // textBoxTemplate
            // 
            this.textBoxTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelApp.SetColumnSpan(this.textBoxTemplate, 3);
            this.textBoxTemplate.Location = new System.Drawing.Point(93, 32);
            this.textBoxTemplate.Name = "textBoxTemplate";
            this.textBoxTemplate.Size = new System.Drawing.Size(264, 20);
            this.textBoxTemplate.TabIndex = 1;
            // 
            // textBoxCurrentFile
            // 
            this.textBoxCurrentFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelApp.SetColumnSpan(this.textBoxCurrentFile, 5);
            this.textBoxCurrentFile.Location = new System.Drawing.Point(93, 116);
            this.textBoxCurrentFile.Name = "textBoxCurrentFile";
            this.textBoxCurrentFile.ReadOnly = true;
            this.textBoxCurrentFile.Size = new System.Drawing.Size(450, 20);
            this.textBoxCurrentFile.TabIndex = 6;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelApp.SetColumnSpan(this.buttonSearch, 2);
            this.buttonSearch.Location = new System.Drawing.Point(363, 31);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(180, 22);
            this.buttonSearch.TabIndex = 8;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // labelCurrentFile
            // 
            this.labelCurrentFile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCurrentFile.AutoSize = true;
            this.labelCurrentFile.Location = new System.Drawing.Point(3, 112);
            this.labelCurrentFile.Name = "labelCurrentFile";
            this.labelCurrentFile.Size = new System.Drawing.Size(84, 28);
            this.labelCurrentFile.TabIndex = 7;
            this.labelCurrentFile.Text = "Current file:";
            this.labelCurrentFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxDirectory
            // 
            this.textBoxDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelApp.SetColumnSpan(this.textBoxDirectory, 5);
            this.textBoxDirectory.Location = new System.Drawing.Point(93, 4);
            this.textBoxDirectory.Name = "textBoxDirectory";
            this.textBoxDirectory.Size = new System.Drawing.Size(450, 20);
            this.textBoxDirectory.TabIndex = 0;
            // 
            // treeViewDirectories
            // 
            this.treeViewDirectories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelApp.SetColumnSpan(this.treeViewDirectories, 6);
            this.treeViewDirectories.ImageIndex = 0;
            this.treeViewDirectories.ImageList = this.imageList;
            this.treeViewDirectories.Location = new System.Drawing.Point(3, 143);
            this.treeViewDirectories.Name = "treeViewDirectories";
            this.tableLayoutPanelApp.SetRowSpan(this.treeViewDirectories, 5);
            this.treeViewDirectories.SelectedImageIndex = 0;
            this.treeViewDirectories.Size = new System.Drawing.Size(540, 280);
            this.treeViewDirectories.TabIndex = 9;
            // 
            // buttonPause
            // 
            this.buttonPause.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelApp.SetColumnSpan(this.buttonPause, 2);
            this.buttonPause.Location = new System.Drawing.Point(363, 59);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(180, 22);
            this.buttonPause.TabIndex = 10;
            this.buttonPause.Text = "Pause";
            this.buttonPause.UseVisualStyleBackColor = true;
            this.buttonPause.Visible = false;
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // textBoxTimer
            // 
            this.textBoxTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTimer.Location = new System.Drawing.Point(453, 88);
            this.textBoxTimer.Name = "textBoxTimer";
            this.textBoxTimer.ReadOnly = true;
            this.textBoxTimer.Size = new System.Drawing.Size(90, 20);
            this.textBoxTimer.TabIndex = 11;
            // 
            // timerElapsed
            // 
            this.timerElapsed.Tick += new System.EventHandler(this.timerElapsed_Tick);
            // 
            // textBoxFileCount
            // 
            this.textBoxFileCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFileCount.Location = new System.Drawing.Point(273, 88);
            this.textBoxFileCount.Name = "textBoxFileCount";
            this.textBoxFileCount.ReadOnly = true;
            this.textBoxFileCount.Size = new System.Drawing.Size(84, 20);
            this.textBoxFileCount.TabIndex = 12;
            // 
            // labelTimer
            // 
            this.labelTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTimer.AutoSize = true;
            this.labelTimer.Location = new System.Drawing.Point(363, 84);
            this.labelTimer.Name = "labelTimer";
            this.labelTimer.Size = new System.Drawing.Size(84, 28);
            this.labelTimer.TabIndex = 13;
            this.labelTimer.Text = "Time elapsed:";
            this.labelTimer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelFileCount
            // 
            this.labelFileCount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFileCount.AutoSize = true;
            this.labelFileCount.Location = new System.Drawing.Point(183, 84);
            this.labelFileCount.Name = "labelFileCount";
            this.labelFileCount.Size = new System.Drawing.Size(84, 28);
            this.labelFileCount.TabIndex = 14;
            this.labelFileCount.Text = "Files processed:";
            this.labelFileCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxFilesFound
            // 
            this.textBoxFilesFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFilesFound.Location = new System.Drawing.Point(93, 88);
            this.textBoxFilesFound.Name = "textBoxFilesFound";
            this.textBoxFilesFound.ReadOnly = true;
            this.textBoxFilesFound.Size = new System.Drawing.Size(84, 20);
            this.textBoxFilesFound.TabIndex = 15;
            // 
            // labelFilesFound
            // 
            this.labelFilesFound.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFilesFound.AutoSize = true;
            this.labelFilesFound.Location = new System.Drawing.Point(3, 84);
            this.labelFilesFound.Name = "labelFilesFound";
            this.labelFilesFound.Size = new System.Drawing.Size(84, 28);
            this.labelFilesFound.TabIndex = 16;
            this.labelFilesFound.Text = "Files found:";
            this.labelFilesFound.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanelApp
            // 
            this.tableLayoutPanelApp.ColumnCount = 6;
            this.tableLayoutPanelApp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelApp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelApp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelApp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelApp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelApp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelApp.Controls.Add(this.labelDirectory, 0, 0);
            this.tableLayoutPanelApp.Controls.Add(this.textBoxTimer, 5, 3);
            this.tableLayoutPanelApp.Controls.Add(this.labelTimer, 4, 3);
            this.tableLayoutPanelApp.Controls.Add(this.labelFileCount, 2, 3);
            this.tableLayoutPanelApp.Controls.Add(this.textBoxFilesFound, 1, 3);
            this.tableLayoutPanelApp.Controls.Add(this.labelFilesFound, 0, 3);
            this.tableLayoutPanelApp.Controls.Add(this.textBoxFileCount, 3, 3);
            this.tableLayoutPanelApp.Controls.Add(this.textBoxDirectory, 1, 0);
            this.tableLayoutPanelApp.Controls.Add(this.labelTemplate, 0, 1);
            this.tableLayoutPanelApp.Controls.Add(this.textBoxCurrentFile, 1, 4);
            this.tableLayoutPanelApp.Controls.Add(this.labelText, 0, 2);
            this.tableLayoutPanelApp.Controls.Add(this.labelCurrentFile, 0, 4);
            this.tableLayoutPanelApp.Controls.Add(this.textBoxText, 1, 2);
            this.tableLayoutPanelApp.Controls.Add(this.buttonPause, 4, 2);
            this.tableLayoutPanelApp.Controls.Add(this.treeViewDirectories, 0, 5);
            this.tableLayoutPanelApp.Controls.Add(this.textBoxTemplate, 1, 1);
            this.tableLayoutPanelApp.Controls.Add(this.buttonSearch, 4, 1);
            this.tableLayoutPanelApp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelApp.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelApp.Name = "tableLayoutPanelApp";
            this.tableLayoutPanelApp.RowCount = 10;
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.33333F));
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.33333F));
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.33333F));
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.33333F));
            this.tableLayoutPanelApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.33333F));
            this.tableLayoutPanelApp.Size = new System.Drawing.Size(546, 426);
            this.tableLayoutPanelApp.TabIndex = 17;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Drive.png");
            this.imageList.Images.SetKeyName(1, "Folder.png");
            this.imageList.Images.SetKeyName(2, "file.png");
            // 
            // FileSearcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 426);
            this.Controls.Add(this.tableLayoutPanelApp);
            this.MinimumSize = new System.Drawing.Size(562, 465);
            this.Name = "FileSearcher";
            this.Text = "File Searcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FileSearcher_FormClosing);
            this.Load += new System.EventHandler(this.FileSearcher_Load);
            this.tableLayoutPanelApp.ResumeLayout(false);
            this.tableLayoutPanelApp.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelDirectory;
        private System.Windows.Forms.Label labelTemplate;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.TextBox textBoxText;
        private System.Windows.Forms.TextBox textBoxTemplate;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Label labelCurrentFile;
        private System.Windows.Forms.TextBox textBoxDirectory;
        private System.Windows.Forms.TextBox textBoxCurrentFile;
        private System.Windows.Forms.TreeView treeViewDirectories;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.TextBox textBoxTimer;
        private System.Windows.Forms.Timer timerElapsed;
        private System.Windows.Forms.TextBox textBoxFileCount;
        private System.Windows.Forms.Label labelTimer;
        private System.Windows.Forms.Label labelFileCount;
        private System.Windows.Forms.TextBox textBoxFilesFound;
        private System.Windows.Forms.Label labelFilesFound;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelApp;
        private System.Windows.Forms.ImageList imageList;
    }
}

